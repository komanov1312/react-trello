import React from 'react';
import './App.css';

function App() {
  // eslint-disable-next-line no-unused-vars
  const [boards, setBoards] = React.useState([
    {
      id: 1,
      title: 'Сделать',
      items: [
        { id: 1, title: 'Пойти в магазин' },
        { id: 2, title: 'Выкинуть мусор' },
        { id: 3, title: 'Покушать' },
      ],
    },
    {
      id: 2,
      title: 'Проверить',
      items: [
        { id: 4, title: 'Сделать код-ревью' },
        { id: 5, title: 'Задача на факториал' },
        { id: 6, title: 'Задачи на фибоначчи' },
      ],
    },
    {
      id: 3,
      title: 'Сделано',
      items: [
        { id: 7, title: 'Снять видео' },
        { id: 8, title: 'Смонтировать' },
        { id: 9, title: 'Отрендерить' },
      ],
    },
  ]);

  const [currentBoard, setCurrentBoard] = React.useState(null);
  const [currentItem, setCurrentItem] = React.useState(null);

  const dragOverHandler = (e) => {
    e.preventDefault();
    if (e.target.className == 'item') {
      e.target.style.boxShadow = '0 2px 3px grey';
    }
  };
  const dragLeaveHandler = (e) => {
    if (e.target.className == 'item') {
      e.target.style.boxShadow = 'none';
    }
  };
  const dragStartHandler = (e, board, item) => {
    setCurrentBoard(board);
    setCurrentItem(item);
  };
  const dragEndHandler = (e) => {
    if (e.target.className == 'item') {
      e.target.style.boxShadow = 'none';
    }
  };
  const dropHandler = (e, board, item) => {
    e.preventDefault();
    const currentIndex = currentBoard.items.indexOf(currentItem);
    currentBoard.items.splice(currentIndex, 1);
    const dropIndex = board.items.indexOf(item);
    board.items.splice(dropIndex + 1, 0, currentItem);
    setBoards(
      boards.map((b) => {
        if (b.id === board.id) {
          return board;
        }
        if (b.id == currentBoard.id) {
          return currentBoard;
        }
        return b;
      })
    );
  };

  const dropCardHandler = (e, board) => {
    board.items.push(currentItem);
    const currentIndex = currentBoard.items.indexOf(currentItem);
    currentBoard.items.splice(currentIndex, 1);
    setBoards(
      boards.map((b) => {
        if (b.id === board.id) {
          return board;
        }
        if (b.id == currentBoard.id) {
          return currentBoard;
        }
        return b;
      })
    );
  };

  return (
    <div className='app'>
      {boards.map((board) => (
        <div
          className='board'
          key={board.id}
          onDragOver={(e) => dragOverHandler(e)}
          onDrop={(e) => dropCardHandler(e, board)}
        >
          <div className='board__title'>{board.title}</div>
          {board.items.map((item) => (
            <div
              className='item'
              key={item.id}
              draggable={true}
              onDragOver={(e) => dragOverHandler(e)}
              onDragLeave={(e) => dragLeaveHandler(e)}
              onDragStart={(e) => dragStartHandler(e, board, item)}
              onDragEnd={(e) => dragEndHandler(e)}
              onDrop={(e) => dropHandler(e, board, item)}
            >
              {item.title}
            </div>
          ))}
        </div>
      ))}
    </div>
  );
}

export default App;
